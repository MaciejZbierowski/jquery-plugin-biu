/* jshint jquery: true, devel: true */
(function($) {
    
    $.fn.validateText = function(options){
        
        return this.each(function(){
            
            var classVar = $(this).attr('class'), fieldToChange = $(this), submitBtn = $('input[type=submit]'), settings, inputsProper = $('input[type=text].proper').length;
            
            $('input[type=text]').val('');
            submitBtn.prop("disabled", true);
            
            if(classVar === 'email')
                settings = $.extend({
                    'pattern' : /^[a-zA-Z!#$%&'*+/=?^{|}~]([a-zA-Z0-9!#$&%'*+\-/=?^_{|}~]+([.]|[a-zA-Z0-9!#$&%'*+\-/=?^_{|}~]*))+@[a-zA-Z0-9]+\.[a-zA-Z]{2}$/
                }, options);
            else if(classVar === 'telephone')
                settings = $.extend({
                    'pattern' : /^[0-9]{9}$/
                }, options);
            else if(classVar === 'pesel')
                settings = $.extend({
                    'pattern' : /^[0-9][0-9]((([0][469]|[1][1])([0-2][0-9]|[3][0]))|(([0][13578]|[1][0]|[1][2])([0-2][0-9]|[3][1]))|([0][2]([2][8]|[2][9])))[0-9]{5}$/
                }, options);
            console.log('pattern == ' + settings.pattern);
            var checkPattern = function(){
                
                if(fieldToChange.is('input[type=text]')){
                    fieldToChange.on("keyup", function(){
                        if(settings.pattern.test(fieldToChange.val()) && fieldToChange.val() !== ''){
                            fieldToChange.css('border', '1px solid black');
                            fieldToChange.addClass('proper');
                            fieldToChange.next('p').css('display', 'none');
                        } else if(fieldToChange.val() === ''){
                            fieldToChange.css('border', '1px solid black');
                            fieldToChange.removeClass('proper');
                            fieldToChange.next('p').css('display', 'none');
                        }
                        else{
                            fieldToChange.css('border', '1px solid red');
                            fieldToChange.next('p').css('display', 'block');
                            fieldToChange.next('p').text('Niepoprawy format');
                        }

                        if($('input[type=text]').length === $('input[type=text].proper').length)
                            submitBtn.prop("disabled", false);
                        else
                            submitBtn.prop("disabled", true);
                    });

                }
                
            };
            
            checkPattern();
            
        });
        
    };
    
})(jQuery);