/* jshint jquery: true, browser: true, devel: true */

$(document).ready(function(){
    
    //$('.email').validateText();
    
    $('.email').validateText({
        pattern: /^[0-9]$/
    });
    
    $('.telephone').validateText();
    
    $('.pesel').validateText();
    
});